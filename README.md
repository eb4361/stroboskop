# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://eb4361@bitbucket.org/eb4361/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/eb4361/stroboskop/commits/753a0ff8f38a8e5bd935fa4edd2e8de394a88ef8

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/eb4361/stroboskop/commits/a59e8649bf41b56c36ff8cb5f8da519ff3aa1246

Naloga 6.3.2:
https://bitbucket.org/eb4361/stroboskop/commits/82c7dff4f55852bf3230c29c8d1625f4e8d2c35e

Naloga 6.3.3:
https://bitbucket.org/eb4361/stroboskop/commits/bc81f31987f77aa999bf073d7e100eb63b9981b9

Naloga 6.3.4:
https://bitbucket.org/eb4361/stroboskop/commits/ee48c085de793c203cde9a376e73a6b7d311da4b

Naloga 6.3.5:

```
git merge master izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/eb4361/stroboskop/commits/8d5ed28fa28a8fa2c9b6bb00bf22e94d273955eb

Naloga 6.4.2:
https://bitbucket.org/eb4361/stroboskop/commits/bfd79a6c77983a1557e50b9d6084a63e992881fd

Naloga 6.4.3:
https://bitbucket.org/eb4361/stroboskop/commits/e9d154ea3b978f7cb01c93ae83ac7ddc63272f98

Naloga 6.4.4:
https://bitbucket.org/eb4361/stroboskop/commits/074c8e1f729366b84e23a543f9c357d46e137fa2